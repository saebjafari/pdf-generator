const fs = require("fs");
const puppeteer = require("puppeteer");
const validation = require("./validation");
class generaratePdf {
  constructor() {
    this.createHtml = this.createHtml.bind(this);
  }

  async createPdf(req, res) {
    const DATA = req.body.data;

    try {
      const validate = await validation.validateAsync(req.body);
      const browser = await puppeteer.launch({
        executablePath: "/usr/bin/chromium-browser",
      });
      const page = await browser.newPage();
      let headerMoreData = [];
      if (req.body?.headerMoreData?.length > 0) {
        headerMoreData = req.body.headerMoreData;
      }

      await this.createHtml(
        page,
        DATA,
        req.body?.name,
        req.body?.date,
        headerMoreData
      );
      page.on("response", async (response) => {
        // don't await anything on this line
        if (response.request().resourceType() === "stylesheet") {
          resolve(JSON.stringify(await response.text())); // await and resolve here
        }
      });

      await page.pdf(
        {
          path: "invoice.pdf",
          format: "A4",
          margin: {
            top: "10px",
            left: "10px",
            right: "10px",
            bottom: "10px",
          },
        },
        { printBackground: true },
        { landscape: true },
        { displayHeaderFooter: true }
      );
      var file = fs.createReadStream("./invoice.pdf");
      var stat = fs.statSync("./invoice.pdf");
      res.setHeader("Content-Length", stat.size);
      res.setHeader("Content-Type", "application/pdf");

      res.setHeader("Content-Disposition", "attachment; filename=RAPORs.pdf");
      setTimeout(() => {
        fs.unlink("./invoice.pdf", (err) => {
          if (err) {
            console.error(err);
            return;
          }
        });
      }, 20);

      file.pipe(res);
      await browser.close();
    } catch (error) {
      res.json({
        IsSuccess: false,
        message: error.message || "somthing went wrong",
      });
    }

    // await browser.close();
  }
  async createHtml(page, DATA, name, date, headerMoreData) {
    return await page.setContent(`<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
        <script src="https://cdn.tailwindcss.com"></script>
        <style type="text/tailwindcss">
          @layer utilities {
            body {
              margin-top: 20px;
              color: #484b51;
            }
            .text-secondary-d1 {
              color: #728299 !important;
            }
            .page-header {
              margin: 0 0 1rem;
              padding-bottom: 1rem;
              padding-top: 0.5rem;
              border-bottom: 1px dotted #e2e2e2;
              display: -ms-flexbox;
              display: flex;
              -ms-flex-pack: justify;
              justify-content: space-between;
              -ms-flex-align: center;
              align-items: center;
            }
            .page-title {
              padding: 0;
              margin: 0;
              font-size: 1.75rem;
              font-weight: 300;
            }
            .brc-default-l1 {
              border-color: #dce9f0 !important;
            }
    
            .ml-n1,
            .mx-n1 {
              margin-left: -0.25rem !important;
            }
            .mr-n1,
            .mx-n1 {
              margin-right: -0.25rem !important;
            }
            .mb-4,
            .my-4 {
              margin-bottom: 1.5rem !important;
            }
    
            hr {
              margin-top: 1rem;
              margin-bottom: 1rem;
              border: 0;
              border-top: 1px solid rgba(0, 0, 0, 0.1);
            }
    
            .text-grey-m2 {
              color: #888a8d !important;
            }
    
            .text-success-m2 {
              color: #86bd68 !important;
            }
    
            .font-bolder,
            .text-600 {
              font-weight: 600 !important;
            }
    
            .text-110 {
              font-size: 110% !important;
            }
            .text-blue {
              color: #478fcc !important;
            }
            .pb-25,
            .py-25 {
              padding-bottom: 0.75rem !important;
            }
    
            .pt-25,
            .py-25 {
              padding-top: 0.75rem !important;
            }
            .bgc-default-tp1 {
              background-color: rgb(7, 62, 186) !important;
            }
            .bgc-default-l4,
            .bgc-h-default-l4:hover {
              background-color: #f3f8fa !important;
            }
            .page-header .page-tools {
              -ms-flex-item-align: end;
              align-self: flex-end;
            }
    
            .btn-light {
              color: #757984;
              background-color: #f5f6f9;
              border-color: #dddfe4;
            }
            .w-2 {
              width: 1rem;
            }
            .page-content {
              min-width: 1700px;
            }
            .text-120 {
              font-size: 120% !important;
            }
            .text-primary-m1 {
              color: #04091c !important;
            }
    
            .text-danger-m1 {
              color: #dd4949 !important;
            }
            .text-blue-m2 {
              color: #68a3d5 !important;
            }
            .text-150 {
              font-size: 150% !important;
            }
            .text-60 {
              font-size: 60% !important;
            }
            .text-grey-m1 {
              color: #7b7d81 !important;
            }
            .align-bottom {
              vertical-align: bottom !important;
            }
          }
        </style>
        <style>
          html {
            -webkit-print-color-adjust: exact !important;
            -webkit-filter: opacity(1) !important;
          }
        </style>
    
        <link
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        />
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
      </head>
      <body>
      <div class="flex justify-center absolute   z-10 opacity-10 -rotate-[60deg] -left-[300px] top-1/3">
      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 148 30"
                        style="height: 430px; width: auto;"
                      >
                        <g fill="#313131" fill-rule="evenodd">
                          <path
                            d="M13.295 27.129c-.171-.757-.378-1.486-.545-2.252.797.035 1.555-.038 2.316-.161.201.75.358 1.486.567 2.224l.969-.232-.54-2.195 2.164-.807.536 2.136c4.278-1.98 7.951-7.488 6.462-13.816-1.54-6.544-7.478-9.729-12.257-9.467l.57 2.31-2.286.315-.593-2.363-.971.259.593 2.401a11.23 11.23 0 0 0-2.13.947L7.542 4C2.904 6.397-.284 12.32 1.617 18.492c1.872 6.074 7.518 8.783 11.678 8.637m5.738-.201c.072.296.142.584.214.871.045.179.067.357.022.539-.064.257-.19.478-.42.611-.338.196-.703.312-1.098.21-.364-.095-.601-.336-.706-.7-.067-.228-.12-.461-.185-.717l-.978.231.162.653c.149.612-.148 1.125-.755 1.285a1.996 1.996 0 0 1-.53.089c-.438-.008-.798-.3-.922-.731-.078-.272-.14-.549-.21-.823-.017-.069-.038-.137-.06-.217-3.563.004-6.681-1.161-9.303-3.587-2.09-1.933-3.412-4.32-3.981-7.115-.597-2.936-.25-5.77 1.043-8.47 1.292-2.702 3.288-4.73 5.937-6.133-.022-.109-.04-.214-.065-.318-.042-.184-.096-.365-.13-.55a.99.99 0 0 1 .69-1.138 3.75 3.75 0 0 1 .379-.095.99.99 0 0 1 1.153.688c.054.172.094.348.144.534l.963-.254c-.04-.17-.081-.336-.116-.503A.987.987 0 0 1 10.735.22c.26-.153.55-.227.85-.218.442.012.796.298.921.73.07.243.128.49.193.744 3.613-.103 6.792 1.01 9.49 3.422 2.105 1.88 3.468 4.223 4.086 6.984.68 3.04.356 5.982-.999 8.788-1.353 2.804-3.452 4.874-6.243 6.259"
                          ></path>
                          <path
                            d="M22.39 15.217c0 .261.008.492-.001.723-.063 1.533-.463 2.962-1.297 4.26a7.916 7.916 0 0 1-2.871 2.689c-1.31.724-2.718 1.134-4.201 1.28a8.155 8.155 0 0 1-4.171-.657 7.744 7.744 0 0 1-2.237-1.51c-1.126-1.082-1.904-2.38-2.416-3.85a12.23 12.23 0 0 1-.623-2.778c-.177-1.72.049-3.377.789-4.948a7.681 7.681 0 0 1 1.363-1.981c1.3-1.388 2.924-2.172 4.748-2.584.942-.213 1.895-.317 2.861-.254 1.823.118 3.421.784 4.792 1.998.841.745 1.482 1.643 1.994 2.64.022.043.037.09.066.163l-4.475 1.12c-.181-.201-.342-.397-.521-.575-.811-.805-1.795-1.208-2.93-1.201a4.629 4.629 0 0 0-2.265.584c-1.009.566-1.587 1.457-1.873 2.557a6.135 6.135 0 0 0-.05 2.442c.167.96.457 1.877.983 2.703.607.953 1.428 1.63 2.533 1.92.704.185 1.404.127 2.098-.063 1.216-.334 2.16-1.02 2.753-2.157a4.28 4.28 0 0 0 .387-1.083l.066-.314 4.498-1.124"
                          ></path>
                          <path
                            d="M32.93 10.174a7.19 7.19 0 0 1 2.796-2.846c1.196-.68 2.552-1.02 4.067-1.02 1.858 0 3.447.494 4.77 1.48 1.322.986 2.206 2.33 2.652 4.033h-4.19c-.312-.657-.754-1.157-1.326-1.501-.572-.344-1.222-.516-1.95-.516-1.174 0-2.125.411-2.853 1.233-.728.822-1.092 1.92-1.092 3.294 0 1.375.364 2.473 1.092 3.294.728.822 1.679 1.233 2.853 1.233.728 0 1.378-.172 1.95-.516.572-.343 1.014-.844 1.326-1.501h4.19c-.446 1.703-1.33 3.044-2.652 4.022-1.323.979-2.912 1.468-4.77 1.468-1.515 0-2.871-.34-4.067-1.02a7.207 7.207 0 0 1-2.797-2.834c-.668-1.21-1.003-2.592-1.003-4.146 0-1.554.335-2.94 1.003-4.157"
                          ></path>
                          <path
                            d="M59.951 17.625c.75-.836 1.126-1.942 1.126-3.316 0-1.39-.376-2.499-1.126-3.328-.75-.83-1.75-1.244-2.997-1.244-1.263 0-2.27.411-3.02 1.233-.75.821-1.125 1.934-1.125 3.339 0 1.389.375 2.498 1.125 3.327.75.83 1.757 1.244 3.02 1.244 1.248 0 2.247-.418 2.997-1.255m-7.042 3.72a7.681 7.681 0 0 1-2.92-2.88c-.72-1.232-1.08-2.618-1.08-4.156 0-1.539.36-2.921 1.08-4.146a7.709 7.709 0 0 1 2.92-2.868c1.226-.687 2.574-1.031 4.045-1.031 1.47 0 2.82.344 4.045 1.03a7.55 7.55 0 0 1 2.897 2.869c.706 1.225 1.059 2.607 1.059 4.146 0 1.538-.357 2.924-1.07 4.156a7.626 7.626 0 0 1-2.897 2.88c-1.219.687-2.563 1.03-4.034 1.03-1.471 0-2.82-.343-4.045-1.03"
                          ></path>
                          <path d="M67.072 22.219h3.811V6.488h-3.811z"></path>
                          <path
                            d="M87.642 22.219h-3.811l-6.374-9.703v9.703h-3.811V6.488h3.811l6.374 9.748V6.488h3.811z"
                          ></path>
                          <path
                            d="M97.916 13.009c.357-.344.535-.83.535-1.457 0-.627-.178-1.113-.535-1.456-.356-.344-.899-.516-1.627-.516h-2.072v3.944h2.072c.728 0 1.27-.171 1.627-.515m3.789 1.042c-.416.754-1.055 1.363-1.917 1.826-.862.463-1.931.695-3.209.695h-2.362v5.647h-3.811V6.488h6.173c1.248 0 2.303.217 3.165.65.861.433 1.508 1.03 1.939 1.792.43.762.646 1.636.646 2.622a5.09 5.09 0 0 1-.624 2.499"
                          ></path>
                          <path
                            d="m111.094 16.482-1.94-5.759-1.916 5.76h3.856Zm.98 2.958h-5.839l-.936 2.779h-3.989l5.66-15.731h4.414l5.66 15.73h-4.034l-.936-2.778Z"
                          ></path>
                          <path
                            d="M122.571 13.57h2.363c.698 0 1.222-.173 1.57-.516.35-.344.525-.83.525-1.457 0-.597-.175-1.068-.524-1.412-.35-.343-.873-.515-1.571-.515h-2.363v3.9Zm4.168 8.649-3.254-5.938h-.914v5.938h-3.81V6.488h6.396c1.233 0 2.284.217 3.153.65.87.433 1.519 1.027 1.95 1.781.43.755.646 1.595.646 2.521 0 1.046-.293 1.98-.88 2.801-.587.822-1.452 1.405-2.596 1.748l3.61 6.23h-4.301Z"
                          ></path>
                          <path
                            d="m142.05 16.482-1.94-5.759-1.916 5.76h3.855Zm.98 2.958h-5.839l-.936 2.779h-3.99l5.661-15.731h4.413L148 22.218h-4.034l-.936-2.778Z"
                          ></path>
                        </g></svg
                    ></div>
        <div class="page-content relative z-50">
        <div class="flex justify-center">
        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 148 30"
                          style="height: 60px; width: auto;"
                        >
                          <g fill="#313131" fill-rule="evenodd">
                            <path
                              d="M13.295 27.129c-.171-.757-.378-1.486-.545-2.252.797.035 1.555-.038 2.316-.161.201.75.358 1.486.567 2.224l.969-.232-.54-2.195 2.164-.807.536 2.136c4.278-1.98 7.951-7.488 6.462-13.816-1.54-6.544-7.478-9.729-12.257-9.467l.57 2.31-2.286.315-.593-2.363-.971.259.593 2.401a11.23 11.23 0 0 0-2.13.947L7.542 4C2.904 6.397-.284 12.32 1.617 18.492c1.872 6.074 7.518 8.783 11.678 8.637m5.738-.201c.072.296.142.584.214.871.045.179.067.357.022.539-.064.257-.19.478-.42.611-.338.196-.703.312-1.098.21-.364-.095-.601-.336-.706-.7-.067-.228-.12-.461-.185-.717l-.978.231.162.653c.149.612-.148 1.125-.755 1.285a1.996 1.996 0 0 1-.53.089c-.438-.008-.798-.3-.922-.731-.078-.272-.14-.549-.21-.823-.017-.069-.038-.137-.06-.217-3.563.004-6.681-1.161-9.303-3.587-2.09-1.933-3.412-4.32-3.981-7.115-.597-2.936-.25-5.77 1.043-8.47 1.292-2.702 3.288-4.73 5.937-6.133-.022-.109-.04-.214-.065-.318-.042-.184-.096-.365-.13-.55a.99.99 0 0 1 .69-1.138 3.75 3.75 0 0 1 .379-.095.99.99 0 0 1 1.153.688c.054.172.094.348.144.534l.963-.254c-.04-.17-.081-.336-.116-.503A.987.987 0 0 1 10.735.22c.26-.153.55-.227.85-.218.442.012.796.298.921.73.07.243.128.49.193.744 3.613-.103 6.792 1.01 9.49 3.422 2.105 1.88 3.468 4.223 4.086 6.984.68 3.04.356 5.982-.999 8.788-1.353 2.804-3.452 4.874-6.243 6.259"
                            ></path>
                            <path
                              d="M22.39 15.217c0 .261.008.492-.001.723-.063 1.533-.463 2.962-1.297 4.26a7.916 7.916 0 0 1-2.871 2.689c-1.31.724-2.718 1.134-4.201 1.28a8.155 8.155 0 0 1-4.171-.657 7.744 7.744 0 0 1-2.237-1.51c-1.126-1.082-1.904-2.38-2.416-3.85a12.23 12.23 0 0 1-.623-2.778c-.177-1.72.049-3.377.789-4.948a7.681 7.681 0 0 1 1.363-1.981c1.3-1.388 2.924-2.172 4.748-2.584.942-.213 1.895-.317 2.861-.254 1.823.118 3.421.784 4.792 1.998.841.745 1.482 1.643 1.994 2.64.022.043.037.09.066.163l-4.475 1.12c-.181-.201-.342-.397-.521-.575-.811-.805-1.795-1.208-2.93-1.201a4.629 4.629 0 0 0-2.265.584c-1.009.566-1.587 1.457-1.873 2.557a6.135 6.135 0 0 0-.05 2.442c.167.96.457 1.877.983 2.703.607.953 1.428 1.63 2.533 1.92.704.185 1.404.127 2.098-.063 1.216-.334 2.16-1.02 2.753-2.157a4.28 4.28 0 0 0 .387-1.083l.066-.314 4.498-1.124"
                            ></path>
                            <path
                              d="M32.93 10.174a7.19 7.19 0 0 1 2.796-2.846c1.196-.68 2.552-1.02 4.067-1.02 1.858 0 3.447.494 4.77 1.48 1.322.986 2.206 2.33 2.652 4.033h-4.19c-.312-.657-.754-1.157-1.326-1.501-.572-.344-1.222-.516-1.95-.516-1.174 0-2.125.411-2.853 1.233-.728.822-1.092 1.92-1.092 3.294 0 1.375.364 2.473 1.092 3.294.728.822 1.679 1.233 2.853 1.233.728 0 1.378-.172 1.95-.516.572-.343 1.014-.844 1.326-1.501h4.19c-.446 1.703-1.33 3.044-2.652 4.022-1.323.979-2.912 1.468-4.77 1.468-1.515 0-2.871-.34-4.067-1.02a7.207 7.207 0 0 1-2.797-2.834c-.668-1.21-1.003-2.592-1.003-4.146 0-1.554.335-2.94 1.003-4.157"
                            ></path>
                            <path
                              d="M59.951 17.625c.75-.836 1.126-1.942 1.126-3.316 0-1.39-.376-2.499-1.126-3.328-.75-.83-1.75-1.244-2.997-1.244-1.263 0-2.27.411-3.02 1.233-.75.821-1.125 1.934-1.125 3.339 0 1.389.375 2.498 1.125 3.327.75.83 1.757 1.244 3.02 1.244 1.248 0 2.247-.418 2.997-1.255m-7.042 3.72a7.681 7.681 0 0 1-2.92-2.88c-.72-1.232-1.08-2.618-1.08-4.156 0-1.539.36-2.921 1.08-4.146a7.709 7.709 0 0 1 2.92-2.868c1.226-.687 2.574-1.031 4.045-1.031 1.47 0 2.82.344 4.045 1.03a7.55 7.55 0 0 1 2.897 2.869c.706 1.225 1.059 2.607 1.059 4.146 0 1.538-.357 2.924-1.07 4.156a7.626 7.626 0 0 1-2.897 2.88c-1.219.687-2.563 1.03-4.034 1.03-1.471 0-2.82-.343-4.045-1.03"
                            ></path>
                            <path d="M67.072 22.219h3.811V6.488h-3.811z"></path>
                            <path
                              d="M87.642 22.219h-3.811l-6.374-9.703v9.703h-3.811V6.488h3.811l6.374 9.748V6.488h3.811z"
                            ></path>
                            <path
                              d="M97.916 13.009c.357-.344.535-.83.535-1.457 0-.627-.178-1.113-.535-1.456-.356-.344-.899-.516-1.627-.516h-2.072v3.944h2.072c.728 0 1.27-.171 1.627-.515m3.789 1.042c-.416.754-1.055 1.363-1.917 1.826-.862.463-1.931.695-3.209.695h-2.362v5.647h-3.811V6.488h6.173c1.248 0 2.303.217 3.165.65.861.433 1.508 1.03 1.939 1.792.43.762.646 1.636.646 2.622a5.09 5.09 0 0 1-.624 2.499"
                            ></path>
                            <path
                              d="m111.094 16.482-1.94-5.759-1.916 5.76h3.856Zm.98 2.958h-5.839l-.936 2.779h-3.989l5.66-15.731h4.414l5.66 15.73h-4.034l-.936-2.778Z"
                            ></path>
                            <path
                              d="M122.571 13.57h2.363c.698 0 1.222-.173 1.57-.516.35-.344.525-.83.525-1.457 0-.597-.175-1.068-.524-1.412-.35-.343-.873-.515-1.571-.515h-2.363v3.9Zm4.168 8.649-3.254-5.938h-.914v5.938h-3.81V6.488h6.396c1.233 0 2.284.217 3.153.65.87.433 1.519 1.027 1.95 1.781.43.755.646 1.595.646 2.521 0 1.046-.293 1.98-.88 2.801-.587.822-1.452 1.405-2.596 1.748l3.61 6.23h-4.301Z"
                            ></path>
                            <path
                              d="m142.05 16.482-1.94-5.759-1.916 5.76h3.855Zm.98 2.958h-5.839l-.936 2.779h-3.99l5.661-15.731h4.413L148 22.218h-4.034l-.936-2.778Z"
                            ></path>
                          </g></svg
                      ></div>
         <div>
          <h6 class="text-xl font-bold mb-4" >${name || ""}</h6>
          <h6 class="text-xl font-bold mb-4" ><span class="text-grey">Tarih<span>  : ${
            date || ""
          }</h6>
          
         ${headerMoreData
           .map((item) => {
             return `<h6 class="text-xl font-bold mb-4 > <span class="text-grey">${item.name}<span> : ${item.value}</h6>`;
           })
           .join("")}
         </div>
             ${DATA.map((item) => {
               return `<div class="flex flex-col mb-16">
               <h1 class="text-3xl font-bold mb-4" >${item.title}</h1>
                          <div
                              class="flex justify-between px-4 text-600 text-white bgc-default-tp1 py-25 bg-red-900" 
                            >
                          
                            ${item.headers
                              .map(
                                (header) =>
                                  `<div class="flex-1">${header}</div>`
                              )
                              .join("")}
                            
                            </div>
                            

                            <div class="text-95 text-secondary-d3">
                          ${item.values
                            .map(
                              (
                                item
                              ) => `<div class="flex justify-between px-4 mb-2 mb-sm-0 py-25">
                              ${item
                                .map(
                                  (data) => `<div class="flex-1">${data}</div>`
                                )
                                .join("")}    
                              </div>`
                            )
                            .join("")} 


                            </div>
                            </div>`;
             }).join("")}   
                  
        </div>
      </body>
    </html>
     `);
  }
}

module.exports = {
  generatePdf: new generaratePdf(),
};
