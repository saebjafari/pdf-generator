const Joi = require("joi");

const validation = Joi.object({
  date: Joi.string().required().error(new Error("Date is required")),
  data: Joi.array().required().error(new Error("data is required")),
  name: Joi.string().required().error(new Error("name is required")),
  headerMoreData: Joi.array().items(
    Joi.object({
      name: Joi.string().required().error(new Error("name is required")),
      value: Joi.string().required().error(new Error("value is required")),
    })
  ),
});

module.exports = validation;
