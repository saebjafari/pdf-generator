const express = require("express");
const app = express();
const cors = require("cors");

const { generatePdf } = require("./src/generatePdf");
const validation = require("./src/validation");
app.use(
  cors()
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post("/report", async (req, res) => {
  try {
    generatePdf.createPdf(req, res);
  } catch (error) {
    console.log(error);
  }
});

app.use((req, res, next) => {
  next(new Error("Not Found"));
});
app.use((err, req, res, next) => {
  res.status(err.status || 400).json({
    message: err.message || err || "Something went wrong",
    success: false,
  });
});
app.listen(6767, () => {
  console.log("Server running on port 6767");
});
